package by.siamionau.coffeemaker.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
//import org.apache.log4j.Logger;

public class CoffeeFilter implements Filter {

	@SuppressWarnings("unused")
	private FilterConfig filterConfig;
//	private static final Logger log = Logger.getLogger(CoffeeFilter.class);

	@Override
	public void init(final FilterConfig filterConfig) {
		this.filterConfig = filterConfig;
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, FilterChain chain) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		chain.doFilter(request, response);
}

	@Override
	public void destroy() {
		
	}
}