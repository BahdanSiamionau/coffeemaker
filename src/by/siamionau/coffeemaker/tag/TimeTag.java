package by.siamionau.coffeemaker.tag;

import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.util.GregorianCalendar;

@SuppressWarnings("serial")
public class TimeTag extends TagSupport {
	
	private static final Logger log = Logger.getLogger(TimeTag.class);
	
	public int doStartTag() throws JspException {
		GregorianCalendar calendar = (GregorianCalendar) GregorianCalendar.getInstance();
		String str = calendar.getTime() + "<br>";
		try {
			JspWriter out = pageContext.getOut();
			out.write(str);
		} catch (IOException ex) {
			log.error(ex.getMessage());
			throw new JspException(ex.getMessage());
		}
		return SKIP_BODY;
	}
}
