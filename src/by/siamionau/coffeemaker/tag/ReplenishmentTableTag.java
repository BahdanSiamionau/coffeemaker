package by.siamionau.coffeemaker.tag;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.BodyTagSupport;
import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class ReplenishmentTableTag extends BodyTagSupport {
	
	private static final Logger log = Logger.getLogger(ReplenishmentTableTag.class);
	private int length;
	private String col2Name;
	private String col3Name;
	private String col4Name;
		
	public int doStartTag() throws JspException {
		try {
			pageContext.getOut().write("<br><table width=\"100%\">");
			pageContext.getOut().write("<tr align=\"center\" bgcolor=\"#999999\">");
			pageContext.getOut().write("<td> ID </td>");
			pageContext.getOut().write("<td> " + col2Name + " </td>");
			pageContext.getOut().write("<td> " + col3Name + " </td>");
			pageContext.getOut().write("<td> " + col4Name + " </td>");
			pageContext.getOut().write("</tr>");
		} catch (IOException ex) {
			log.error(ex.getMessage());
			throw new JspException(ex.getMessage());
		} 
		return EVAL_BODY_INCLUDE;
	}
	
	public int doAfterBody() throws JspTagException {
		if (length-- > 1) {
			try {
				pageContext.getOut().write("</TD></TR><TR><TD>");
			} catch (IOException e) {
				throw new JspTagException(e.getMessage());
			}
			return EVAL_BODY_AGAIN;
		} else {
			return SKIP_BODY;
		}
	}
	
	public int doEndTag() throws JspTagException {
		try {
			pageContext.getOut().write("</tr></td>");
			pageContext.getOut().write("</table>");
		} catch (IOException ex) {
			log.error(ex.getMessage());
			throw new JspTagException(ex.getMessage());
		}
		return SKIP_BODY;
	}
	
	public void setLength(String length) {
		this.length = new Integer(length);
	}

	public void setSecondcolumn(String name) {
		this.col2Name = name;
	}
	
	public void setThirdcolumn(String name) {
		this.col3Name = name;
	}
	
	public void setForthcolumn(String name) {
		this.col4Name = name;
	}
}