package by.siamionau.coffeemaker.pool;

import java.util.concurrent.ArrayBlockingQueue;
import by.siamionau.coffeemaker.connector.DatabaseConnector;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class ConnectionPool {

	private static volatile ConnectionPool INSTANCE;
	private ArrayBlockingQueue <DatabaseConnector> connectors;
	private final static int MAX_CONNECTORS = 10;	

	private ConnectionPool() throws CoffeeTechnicException {
		try {
			connectors = new ArrayBlockingQueue<DatabaseConnector>(MAX_CONNECTORS);
			for (int i = 0; i < MAX_CONNECTORS; i++) {
				DatabaseConnector connector = new DatabaseConnector();
				connectors.add(connector);
			}
		} catch (CoffeeTechnicException ex) {
			throw ex;
		}
	}

	public DatabaseConnector take() throws CoffeeTechnicException {
		try {
			DatabaseConnector connector = connectors.take();
			return connector;
		} catch (InterruptedException ex) {
			throw new CoffeeTechnicException(ex);
		}
	}

	public void release(DatabaseConnector connector) {
		if (connectors.size() < MAX_CONNECTORS) {
			connectors.add(connector);
		}
	}
	
	public static ConnectionPool getInstance() throws CoffeeTechnicException {
		if (INSTANCE == null) {
			synchronized (ConnectionPool.class) {
				if (INSTANCE == null) {
					INSTANCE = new ConnectionPool();
				}
			}
		}
		return INSTANCE;
	}
}
