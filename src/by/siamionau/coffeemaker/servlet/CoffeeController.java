package by.siamionau.coffeemaker.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import by.siamionau.coffeemaker.command.Command;
import by.siamionau.coffeemaker.command.RequestHelper;
import by.siamionau.coffeemaker.exception.CoffeeLogicException;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

@WebServlet("/SignController")
public class CoffeeController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(CoffeeController.class);

	public CoffeeController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		performTask(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		performTask(request, response);
	}

	private void performTask(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			RequestHelper requestHelper = RequestHelper.getInstance();
			Command command = requestHelper.getCommand(request);
			String page = command.execute(request); 
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
			dispatcher.forward(request, response);
		} catch (CoffeeTechnicException | CoffeeLogicException | ServletException ex) {
			log.error(ex.getMessage());
			throw new ServletException(ex);
		}
	}
}
