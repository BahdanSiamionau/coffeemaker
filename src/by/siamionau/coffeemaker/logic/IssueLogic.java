package by.siamionau.coffeemaker.logic;

import by.siamionau.coffeemaker.dao.AdditionDAO;
import by.siamionau.coffeemaker.dao.ComponentDAO;
import by.siamionau.coffeemaker.dao.DrinkDAO;
import by.siamionau.coffeemaker.dao.OrderDAO;
import by.siamionau.coffeemaker.dao.UserDAO;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class IssueLogic {

	public Integer makeOrder(String login, String sugar, String drink, boolean milk, boolean chocolate) throws CoffeeTechnicException {
		OrderDAO orderDAO = new OrderDAO();
		return orderDAO.createOrder(login, sugar, drink, milk, chocolate);
	}
	
	public boolean enoughFunds(String login, String drink) throws CoffeeTechnicException {
		UserDAO userDAO = new UserDAO();
		int balance = userDAO.getBalance(login);
		DrinkDAO drinkDAO = new DrinkDAO();
		int cost = drinkDAO.getCost(drink);
		if (balance > cost) {
			return true;
		}
		return false;
	}
	
	public void makeDebit(String login, String drink) throws CoffeeTechnicException {
		DrinkDAO drinkDAO = new DrinkDAO();
		int cost = drinkDAO.getCost(drink);
		UserDAO userDAO = new UserDAO();
		userDAO.debitFunds(login, cost);
	}
	
	public void completeOrder(int id) throws CoffeeTechnicException {
		OrderDAO orderDAO = new OrderDAO();
		orderDAO.setIssued(id);
	}
	
	public boolean enoughComponents(String drink) throws CoffeeTechnicException {
		ComponentDAO componentDAO = new ComponentDAO();
		return componentDAO.enoughComponent(drink);
	}
	
	public boolean enoughAdditions(String sugar, String milk, String chocolate) throws CoffeeTechnicException {
		AdditionDAO additionDAO = new AdditionDAO();
		if (additionDAO.enoughSugar(sugar) 
				&& additionDAO.enoughMilk(milk) 
				&& additionDAO.enoughChocolate(chocolate)) {
			return true;
		}
		return false;
	}
	
	public void takeComponent(String drink) throws CoffeeTechnicException {
		ComponentDAO componentDAO = new ComponentDAO();
		componentDAO.debitComponent(drink);
	}
	
	public void takeSugar(String sugar) throws CoffeeTechnicException {
		AdditionDAO additionDAO = new AdditionDAO();
		additionDAO.takeSugar(Integer.parseInt(sugar));
	}
	
	public void takeMilk(String milk) throws CoffeeTechnicException {
		if (milk != null) {
			AdditionDAO additionDAO = new AdditionDAO();
			additionDAO.takeMilk();
		}
	}
	
	public void takeChocolate(String chocolate) throws CoffeeTechnicException {
		if (chocolate != null) {
			AdditionDAO additionDAO = new AdditionDAO();
			additionDAO.takeChocolate();
		}
	}
}
