package by.siamionau.coffeemaker.logic;

import by.siamionau.coffeemaker.dao.ComponentDAO;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class ReplenishmentLogic {

	public void replenishComponent(String component, String portion) throws CoffeeTechnicException {
		ComponentDAO componentDAO = new ComponentDAO();
		switch (component) {
		case "Sugar":
			componentDAO.addSugar(Integer.parseInt(portion));
			break;
		case "Milk":
			componentDAO.addMilk(Integer.parseInt(portion));
			break;
		case "Chocolate":
			componentDAO.addChocolate(Integer.parseInt(portion));
			break;
		default:
			componentDAO.addComponent(component, Integer.parseInt(portion));
			break;
		}
	}
}
