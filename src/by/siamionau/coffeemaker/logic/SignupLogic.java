package by.siamionau.coffeemaker.logic;

import java.util.List;
import by.siamionau.coffeemaker.dao.UserDAO;
import by.siamionau.coffeemaker.entity.CoffeeUser;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class SignupLogic {
	
	public boolean checkLogin(String login, String password) throws CoffeeTechnicException {
			UserDAO dao = new UserDAO();
			List <CoffeeUser> userList = dao.getUsers();
			for (CoffeeUser user : userList) {
				if (user.getLogin().equals(login)
						&& user.getPassword().equals(password)) {
							return true;
				}
			}
			return false;
	}
	
	public boolean isAdmin(String login) throws CoffeeTechnicException {
		UserDAO dao;
			dao = new UserDAO();
			List <CoffeeUser> userList = dao.getUsers();
			for (CoffeeUser user : userList) {
				if (user.getLogin().equals(login)) {
					if (user.getIsAdmin()) {
						return true;
					}
				}
			}
			return false;
	}
}
