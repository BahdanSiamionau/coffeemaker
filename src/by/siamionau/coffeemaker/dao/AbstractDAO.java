package by.siamionau.coffeemaker.dao;

import by.siamionau.coffeemaker.connector.DatabaseConnector;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public abstract class AbstractDAO {
	
	protected DatabaseConnector connector;
	
	public AbstractDAO() throws CoffeeTechnicException {
		connector = ConnectionPool.getInstance().take();
	}
}