package by.siamionau.coffeemaker.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import by.siamionau.coffeemaker.entity.Drink;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public class DrinkDAO extends AbstractDAO {

	private static final String SQL_SELECT_ALL_DRINKS = "select * from DRINK";
	private static final String SQL_GET_COST = "select COST from DRINK where NAME = \"%1$2s\";";
	private Iterator <Drink> iterator;

	public DrinkDAO() throws CoffeeTechnicException {
		super();
	}

	public List <Drink> getDrinks() throws CoffeeTechnicException {
		List <Drink> drinks = new ArrayList <Drink> ();
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_DRINKS);

			while (resultSet.next()) {
				Drink drink = new Drink();
				drink.setId(resultSet.getInt("ID"));
				drink.setDrinkName(resultSet.getString("NAME"));
				drink.setCost(resultSet.getInt("COST"));
				drinks.add(drink);
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return drinks;
	}


	public String getSize() throws CoffeeTechnicException {
		iterator = getDrinks().iterator();
		return Integer.toString(getDrinks().size());
	}

	public String getElement() {
		Drink drink = iterator.next();
		return new String("<tr align=\"center\"><td>" 
				+ drink.getId() + "</td><td><input type=\"submit\" name=\"drink\" value=\"" 
				+ drink.getDrinkName() + "\"></td><td>" + drink.getCost() + "</td></tr>");
	}

	public Integer getCost(String drink) throws CoffeeTechnicException {
		Integer cost = new Integer(0);
		try (Statement statement = connector.getStatement()) {
			String query = String.format(SQL_GET_COST, drink);
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				cost = resultSet.getInt("COST");
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return cost;
	}
}