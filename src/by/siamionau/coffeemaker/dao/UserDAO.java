package by.siamionau.coffeemaker.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.siamionau.coffeemaker.entity.CoffeeUser;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public class UserDAO extends AbstractDAO {

	private static final String SQL_SELECT_ALL_USERS = "select * from PERSON";
	private static final String SQL_DEBIT_FUNDS = "update PERSON set BALANCE = %1$2s where LOGIN = \"%2$2s\"";
	private static final String SQL_GET_BALANCE = "select BALANCE from PERSON where LOGIN = \"%1$2s\";";
	
	public UserDAO() throws CoffeeTechnicException {
		super();
	}

	public List <CoffeeUser> getUsers() throws CoffeeTechnicException {
		List <CoffeeUser> users = new ArrayList <CoffeeUser> ();
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_USERS);

			while (resultSet.next()) {
				CoffeeUser user = new CoffeeUser();
				user.setId(resultSet.getInt("ID"));
				user.setLogin(resultSet.getString("LOGIN"));
				user.setBalance(resultSet.getInt("BALANCE"));
				user.setPassword(resultSet.getString("PASSWORD"));
				user.setAdmin(resultSet.getBoolean("IS_ADMINISTRATOR"));
				users.add(user);
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return users;
	}

	public void debitFunds(String login, int cost) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int balance = 0;
			String query = String.format(SQL_GET_BALANCE, login);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				balance = resultSet.getInt("BALANCE");
				balance -= cost;
			}
			query = String.format(SQL_DEBIT_FUNDS, balance, login);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
	
	public Integer getBalance(String login) throws CoffeeTechnicException {
		Integer balance = new Integer(0);
		try (Statement statement = connector.getStatement()) {
			String query = String.format(SQL_GET_BALANCE, login);
			ResultSet resultSet = statement.executeQuery(query);

			while (resultSet.next()) {
				balance = resultSet.getInt("BALANCE");
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return balance;
	}
}