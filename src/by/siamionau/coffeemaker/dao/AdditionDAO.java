package by.siamionau.coffeemaker.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import by.siamionau.coffeemaker.entity.Addition;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public class AdditionDAO extends AbstractDAO {

	private static final String SQL_SELECT_ALL_ADDITIONS = "select * from ADDITION";
	private static final String SQL_CHECK_SUGAR = "select QUANTITY from ADDITION where NAME = 'Sugar';";
	private static final String SQL_CHECK_MILK = "select QUANTITY from ADDITION where NAME = 'Milk';";
	private static final String SQL_CHECK_CHOCOLATE = "select QUANTITY from ADDITION where NAME = 'Chocolate';";
	private static final String SQL_UPDATE_SUGAR = "update ADDITION set QUANTITY = %1$2s where NAME = 'Sugar';";
	private static final String SQL_UPDATE_MILK = "update ADDITION set QUANTITY = %1$2s where NAME = 'Milk';";
	private static final String SQL_UPDATE_CHOCOLATE = "update ADDITION set QUANTITY = %1$2s where NAME = 'Chocolate';";

	public AdditionDAO() throws CoffeeTechnicException {
		super();
	}
	
	public List <Addition> getAdditions() throws CoffeeTechnicException {
		List <Addition> additions = new ArrayList <Addition> ();
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ADDITIONS);
			while (resultSet.next()) {
				Addition addition = new Addition();
				addition.setId(resultSet.getInt("ID"));
				addition.setAdditionName(resultSet.getString("NAME"));
				addition.setQuantity(resultSet.getInt("QUANTITY"));
				additions.add(addition);
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return additions;
	}

	public boolean enoughSugar(String sugar) throws CoffeeTechnicException {
		int num = Integer.parseInt(sugar);
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_CHECK_SUGAR);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				if (num < quantity) {
					return true;
				}
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return false;
	}
	
	public boolean enoughMilk(String milk) throws CoffeeTechnicException {
		if (milk == null) {
			return true;
		}
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_CHECK_MILK);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				if (quantity > 0) {
					return true;
				}
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return false;
	}
	
	public boolean enoughChocolate(String chocolate) throws CoffeeTechnicException {
		if (chocolate == null) {
			return true;
		}
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_CHECK_CHOCOLATE);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				if (quantity > 0) {
					return true;
				}
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return false;
	}

	public void takeSugar(int sugar) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_SUGAR);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				newQuantity = quantity - sugar;
			}
			query = String.format(SQL_UPDATE_SUGAR, newQuantity);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
	
	public void takeMilk() throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_MILK);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				newQuantity = quantity - 1;
			}
			query = String.format(SQL_UPDATE_MILK, newQuantity);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
	
	public void takeChocolate() throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_CHOCOLATE);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				newQuantity = quantity - 1;
			}
			query = String.format(SQL_UPDATE_CHOCOLATE, newQuantity);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
}