package by.siamionau.coffeemaker.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import by.siamionau.coffeemaker.entity.Addition;
import by.siamionau.coffeemaker.entity.CoffeeComponent;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public class ComponentDAO extends AbstractDAO {

	private static final String SQL_SELECT_ALL_COMPONENTS = "select COMPONENT.ID, NAME as DRINK_NAME, QUANTITY from COMPONENT, DRINK where COMPONENT.DRINK_ID = DRINK.ID;";
	private static final String SQL_CHECK_COMPONENT = "select QUANTITY from COMPONENT, DRINK where DRINK.ID = DRINK_ID and DRINK.NAME = '%1$2s';";
	private static final String SQL_UPDATE_COMPONENT = "update COMPONENT, DRINK set QUANTITY = %1$2s where DRINK_ID = DRINK.ID and DRINK.NAME = '%2$2s';";
	private static final String SQL_CHECK_SUGAR = "select QUANTITY from ADDITION where NAME = 'Sugar';";
	private static final String SQL_CHECK_MILK = "select QUANTITY from ADDITION where NAME = 'Milk';";
	private static final String SQL_CHECK_CHOCOLATE = "select QUANTITY from ADDITION where NAME = 'Chocolate';";
	private static final String SQL_UPDATE_SUGAR = "update ADDITION set QUANTITY = %1$2s where NAME = 'Sugar';";
	private static final String SQL_UPDATE_MILK = "update ADDITION set QUANTITY = %1$2s where NAME = 'Milk';";
	private static final String SQL_UPDATE_CHOCOLATE = "update ADDITION set QUANTITY = %1$2s where NAME = 'Chocolate';";
	private Iterator <CoffeeComponent> iteratorC;
	private Iterator <Addition> iteratorA;
	private int id = 0;

	public ComponentDAO() throws CoffeeTechnicException {
		super();
	}

	public List <CoffeeComponent> getComponents() throws CoffeeTechnicException {
		List <CoffeeComponent> components = new ArrayList <CoffeeComponent> ();
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_COMPONENTS);
			while (resultSet.next()) {
				CoffeeComponent component = new CoffeeComponent();
				component.setId(resultSet.getInt("ID"));
				component.setDrinkName(resultSet.getString("DRINK_NAME"));
				component.setQuantity(resultSet.getInt("QUANTITY"));
				components.add(component);
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return components;
	}

	public boolean enoughComponent(String drink) throws CoffeeTechnicException {
		boolean result = false;
		try (Statement statement = connector.getStatement()) {
			String query = String.format(SQL_CHECK_COMPONENT, drink);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				if (quantity > 0) {
					result = true;
				}
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return result;
	}

	public void debitComponent(String drink) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_COMPONENT, drink);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				int quantity = resultSet.getInt("QUANTITY");
				newQuantity = quantity - 1;
			}
			query = String.format(SQL_UPDATE_COMPONENT, newQuantity, drink);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}

	public String getSize() throws CoffeeTechnicException {
		AdditionDAO dao = new AdditionDAO();
		iteratorC = getComponents().iterator();
		iteratorA = dao.getAdditions().iterator();
		return Integer.toString(getComponents().size() + dao.getAdditions().size());
	}

	public String getElement() {
		id++;
		if(iteratorC.hasNext()) {
			CoffeeComponent component = iteratorC.next();
			return new String("<tr align=\"center\"><td>" 
					+ id + "</td><td><input type=\"submit\" name=\"component\" value=\"" 
					+ component.getDrinkName() + "\"></td><td><input type=\"text\" class=\"validate[custom[integer],min[1],max[100]]\" id=\"" + component.getDrinkName() + "Portion\" name=\"" + component.getDrinkName() + "Portion\"></td><td>" 
					+ component.getQuantity() + "</td></tr>");
		}
		Addition addition = iteratorA.next();
		return new String("<tr align=\"center\"><td>" 
				+ id + "</td><td><input type=\"submit\" name=\"component\" value=\"" 
				+ addition.getAdditionName() + "\"></td><td><input type=\"text\" name=\"" + addition.getAdditionName() + "Portion\"></td><td>" 
				+ addition.getQuantity() + "</td></tr>");	
	}

	public void addComponent(String component, int portion) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_COMPONENT, component);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				newQuantity = portion + resultSet.getInt("QUANTITY");
			}
			if (newQuantity != 0) {
				query = String.format(SQL_UPDATE_COMPONENT, newQuantity, component);
			}
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}	

	public void addSugar(int portion) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_SUGAR);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				newQuantity = portion + resultSet.getInt("QUANTITY");
			}
			if (newQuantity != 0) {
				query = String.format(SQL_UPDATE_SUGAR, newQuantity);
			}
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}

	public void addMilk(int portion) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_MILK);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				newQuantity = portion + resultSet.getInt("QUANTITY");
			}
			if (newQuantity != 0) {
				query = String.format(SQL_UPDATE_MILK, newQuantity);
			}
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}

	public void addChocolate(int portion) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			int newQuantity = 0;
			String query = String.format(SQL_CHECK_CHOCOLATE);
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()) {
				newQuantity = portion + resultSet.getInt("QUANTITY");
			}
			if (newQuantity != 0) {
				query = String.format(SQL_UPDATE_CHOCOLATE, newQuantity);
			}
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
}
