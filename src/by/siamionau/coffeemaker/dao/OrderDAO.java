package by.siamionau.coffeemaker.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import by.siamionau.coffeemaker.entity.Order;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.pool.ConnectionPool;

public class OrderDAO extends AbstractDAO {
	
	private static final String SQL_SELECT_ALL_ORDERS = "select ORDER.ID, PERSON.LOGIN as PERSON, DRINK.NAME as DRINK, SUGAR_PORTIONS, ISSUED, MILK, CHOCOLATE from DRINK, PERSON, COFFEMAKER.ORDER where PERSON_ID = PERSON.ID and ORDER.DRINK_ID = DRINK.ID order by ID;";
	private static final String SQL_NEW_ORDER = "insert into COFFEMAKER.ORDER select null, PERSON.ID, DRINK.ID, %2$2s, 0, %4$2s, %5$2s from PERSON, DRINK where PERSON.LOGIN = '%1$2s' and DRINK.NAME = '%3$2s';";
	private static final String SQL_LAST_ID = "select max(ID) from COFFEMAKER.ORDER;";
	private static final String SQL_SET_ISSUED = "update COFFEMAKER.ORDER set ISSUED = true where ID = %1$2s;";
	private List <Order> orders;

	public OrderDAO() throws CoffeeTechnicException {
		super();
	}
	
	public List <Order> getOrders() throws CoffeeTechnicException {
		orders = new ArrayList <Order> ();
		try (Statement statement = connector.getStatement()) {
			ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_ORDERS);

			while (resultSet.next()) {
				Order order = new Order();
				order.setId(resultSet.getInt("ID"));
				order.setLogin(resultSet.getString("PERSON"));
				order.setDrink(resultSet.getString("DRINK"));
				order.setSugar(resultSet.getInt("SUGAR_PORTIONS"));
				order.setIssued(resultSet.getBoolean("ISSUED"));
				order.setMilk(resultSet.getBoolean("MILK"));
				order.setChocolate(resultSet.getBoolean("CHOCOLATE"));
				orders.add(order);
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return orders;
	}
	
	public Integer createOrder(String login, String sugar, String drink, boolean milk, boolean chocolate) throws CoffeeTechnicException {
		int result = 0;
		try (Statement statement = connector.getStatement()) {
			String query = String.format(SQL_NEW_ORDER, login, sugar, drink, milk, chocolate);
			statement.executeUpdate(query);
			ResultSet resultSet = statement.executeQuery(SQL_LAST_ID);
			
			while (resultSet.next()) {
				result = resultSet.getInt("max(ID)");
			}
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
		return result;
	}
	
	public void setIssued(int id) throws CoffeeTechnicException {
		try (Statement statement = connector.getStatement()) {
			String query = String.format(SQL_SET_ISSUED, id);
			statement.executeUpdate(query);
		} catch (SQLException ex) {
			throw new CoffeeTechnicException(ex);
		} finally {
			ConnectionPool.getInstance().release(connector);
		}
	}
}
