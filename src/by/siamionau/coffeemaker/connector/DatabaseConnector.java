package by.siamionau.coffeemaker.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class DatabaseConnector {
	
	private Connection connection;

	public DatabaseConnector() throws CoffeeTechnicException {
		try {
			ResourceBundle resource = ResourceBundle.getBundle("properties.database");
			String url = resource.getString("url");
			String user = resource.getString("user");
			String pass = resource.getString("password");
			Properties prop = new Properties();
			prop.put("user", user);
			prop.put("password", pass);
			connection = DriverManager.getConnection(url, prop);
		} catch (MissingResourceException | SQLException ex) {
			throw new CoffeeTechnicException(ex);
		}
	}
	
	public Statement getStatement() throws SQLException, CoffeeTechnicException {
		if (connection != null) {
			return connection.createStatement();
		}
		throw new CoffeeTechnicException("Connection is null");
	}
}