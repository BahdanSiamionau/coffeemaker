package by.siamionau.coffeemaker.entity;

public class Order {
	
	private int id;
	private String login;
	private String drink;
	private int sugar;
	private boolean issued;
	private boolean milk;
	private boolean chocolate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getDrink() {
		return drink;
	}
	public void setDrink(String drink) {
		this.drink = drink;
	}
	public int getSugar() {
		return sugar;
	}
	public void setSugar(int sugar) {
		this.sugar = sugar;
	}
	public boolean isIssued() {
		return issued;
	}
	public void setIssued(boolean issued) {
		this.issued = issued;
	}
	public boolean getMilk() {
		return milk;
	}
	public void setMilk(boolean milk) {
		this.milk = milk;
	}
	public boolean getChocolate() {
		return chocolate;
	}
	public void setChocolate(boolean chocolate) {
		this.chocolate = chocolate;
	}
}