package by.siamionau.coffeemaker.entity;

public class Addition {

	private int id;
	private String additionName;
	private int quantity;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAdditionName() {
		return additionName;
	}
	public void setAdditionName(String addition) {
		this.additionName = addition;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
