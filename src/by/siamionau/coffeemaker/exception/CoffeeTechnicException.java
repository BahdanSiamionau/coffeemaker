package by.siamionau.coffeemaker.exception;

public class CoffeeTechnicException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CoffeeTechnicException(String msg) {
		super(msg);
	}
	
	public CoffeeTechnicException(Exception ex) {
		super(ex);
	}
}
