package by.siamionau.coffeemaker.exception;

public class CoffeeLogicException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public CoffeeLogicException(String msg) {
		super(msg);
	}
	
	public CoffeeLogicException(Exception ex) {
		super(ex);
	}
}
