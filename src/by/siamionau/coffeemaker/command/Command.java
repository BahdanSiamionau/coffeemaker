package by.siamionau.coffeemaker.command;

import javax.servlet.http.HttpServletRequest;
import by.siamionau.coffeemaker.exception.CoffeeLogicException;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public interface Command {
	String execute(HttpServletRequest request) throws CoffeeTechnicException, CoffeeLogicException;
}	