package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle; 
import javax.servlet.http.HttpServletRequest;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;

public class LogoutCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) 
			throws CoffeeTechnicException {
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");
		request.getSession().invalidate();
		return pathProperties.getString("index");
	}

}
