package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;
import by.siamionau.coffeemaker.exception.CoffeeLogicException;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.logic.ReplenishmentLogic;

public class ReplenishmentCommand implements Command {

	final static String PORTION = "[1-9]{1}[0-9]?";
	
	@Override
	public String execute(HttpServletRequest request) throws CoffeeTechnicException, CoffeeLogicException {
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");

		String login = null;
		login = (String)request.getSession().getAttribute("user");
		if (login == null) {
			return pathProperties.getString("index");
		}
		String component = request.getParameter("component");
		String portion = request.getParameter(new String(component + "Portion"));

		ReplenishmentLogic logic = new ReplenishmentLogic();
		if (!portion.isEmpty()) {
			if (!Pattern.compile(PORTION).matcher(portion).matches()) {
				throw new CoffeeLogicException("Incorrect portion value");
			}
			logic.replenishComponent(component, portion);
		} else {
			throw new CoffeeLogicException("Incorrect portion value");
		}
		return pathProperties.getString("success.replenishment");
	}
}
