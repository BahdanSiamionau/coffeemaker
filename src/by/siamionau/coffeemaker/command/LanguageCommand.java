package by.siamionau.coffeemaker.command;

import javax.servlet.http.HttpServletRequest;

public class LanguageCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) {
		request.getSession().setAttribute("language", request.getParameter("language"));
		return request.getParameter("path").substring(request.getParameter("path").indexOf("/Coffeemaker") + 12);
	}
}
