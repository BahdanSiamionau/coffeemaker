package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

public class NoCommand implements Command {
	public String execute(HttpServletRequest request) {
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");
		String page = pathProperties.getString("index");
		return page;
	}
}
