package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle; 
import javax.servlet.http.HttpServletRequest;

import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.logic.IssueLogic;

public class IssueCommand implements Command {

	@Override
	public String execute(HttpServletRequest request) throws CoffeeTechnicException {
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");
		
		String login = null;
		login = (String)request.getSession().getAttribute("user");
		if (login == null) {
			return pathProperties.getString("index");
		}
		String sugar = request.getParameter("sugar");
		String drink = request.getParameter("drink");
		String milk = null;
		milk = request.getParameter("milk");
		String chocolate = null;
		chocolate = request.getParameter("chocolate");
		
		IssueLogic logic = new IssueLogic();
		int id = logic.makeOrder(login, sugar, drink, milk == null ? false : true, chocolate == null ? false : true);
		if (id == 0) {
			return pathProperties.getString("error.order");
		}
		if (!logic.enoughAdditions(sugar, milk, chocolate)) {
			return pathProperties.getString("error.components");
		}
		if (!logic.enoughFunds(login, drink)) {
			return pathProperties.getString("error.balance");
		}
		if (!logic.enoughComponents(drink)) {
			return pathProperties.getString("error.components");
		}
		logic.takeComponent(drink);
		logic.takeSugar(sugar);
		logic.takeMilk(milk);
		logic.takeChocolate(chocolate);
		logic.makeDebit(login, drink);
		logic.completeOrder(id);
		return pathProperties.getString("success.issue");
	}
}
