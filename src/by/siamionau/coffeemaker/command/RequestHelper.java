package by.siamionau.coffeemaker.command;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

public class RequestHelper {
	
	private static RequestHelper instance = null;
	private HashMap <String, Command> commands = new HashMap <String, Command> ();
	
	private RequestHelper() {
		commands.put("login", new SignupCommand());
		commands.put("session", new SessionCommand());
		commands.put("issue", new IssueCommand());
		commands.put("replenishment", new ReplenishmentCommand());
		commands.put("language", new LanguageCommand());
		commands.put("logout", new LogoutCommand());
	}
	
	public Command getCommand(HttpServletRequest request) {
		String action = request.getParameter("command");
		Command command = commands.get(action);
		if (command == null) {
			command = new NoCommand();
		}
		return command;
	}

	public static RequestHelper getInstance() {
		if (instance == null) {
			instance = new RequestHelper();
		}
		return instance;
	}
}

