package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

import by.siamionau.coffeemaker.exception.CoffeeLogicException;
import by.siamionau.coffeemaker.exception.CoffeeTechnicException;
import by.siamionau.coffeemaker.logic.SignupLogic;

public class SignupCommand implements Command {
	
	public String execute(HttpServletRequest request) throws CoffeeTechnicException, CoffeeLogicException {
		String page = null;
		String login = null;
		String pass = null;
		login = request.getParameter("login");
		pass = request.getParameter("password");
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");
		
		if (login != null && pass != null) {
			if (login.length() < 4 || login.length() > 16) {
				throw new CoffeeLogicException("Incorrect login length");
			}
		} else {
			throw new CoffeeLogicException("Incorrect login or password");
		}
		
		SignupLogic logic = new SignupLogic();
		if (logic.checkLogin(login, pass)) {
			request.getSession().setAttribute("user", login);
			if (logic.isAdmin(login)) {
				request.getSession().setAttribute("role", "admin");
			} else {
				request.getSession().setAttribute("role", "user");
			}
			page = pathProperties.getString("user");
		} else {
			page = pathProperties.getString("unsuccess");
		}
		return page;
	}
}