package by.siamionau.coffeemaker.command;

import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;

public class SessionCommand implements Command {

	public String execute(HttpServletRequest request) {
		String session = null; 
		session = request.getParameter("session");
		ResourceBundle pathProperties = ResourceBundle.getBundle("properties.path");

		if (session == null) {
			session = (String)request.getSession().getAttribute("role");
		}	
		switch (session) {
		case "user":
			return pathProperties.getString("user");
		case "admin":
			return pathProperties.getString("replenishment");
		case "replenishment":
			return pathProperties.getString("replenishment");
		case "drinks":
			return pathProperties.getString("user");
		case "allissues":
			return pathProperties.getString("allissues");
		case "allusers":
			return pathProperties.getString("allusers");
		case "myissues":
			return pathProperties.getString("myissues");
		default:
			return pathProperties.getString("index");
		}	
	}
}
