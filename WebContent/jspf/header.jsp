<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
	<link rel="stylesheet" href="css/style.css"
	type="text/css" />
</head>
<body>
	<fmt:setLocale value="${language}" />
	<fmt:setBundle basename="properties.labels" var="labels" /> <br>
	<div id="logout">
	<fmt:message key="success_signup" bundle="${labels}"/>
	<c:out value=" ${user}."/>
	<c:choose>
		<c:when test="${'admin' == role}">
			<fmt:message key="admin" bundle="${labels}"/>
		</c:when>
		<c:when test="${'user' == role}">
			<fmt:message key="user" bundle="${labels}"/>
		</c:when>
	</c:choose>
	<form action="login" method="post">
		<input type="hidden" name="command" value="logout"> 
		<input type="submit" value=<fmt:message key="logout" bundle="${labels}"/>>
	</form>
	</div> 
	<div id="flags"><form action="login" method="post">
		<input type="hidden" name="command" value="language">
		<input type="hidden" name="path" value="${pageContext.request.requestURL}">
		<input class="flag" type="image" src="${pageContext.request.contextPath}/img/en.gif" height="20" width="40" name="language" value="en"> 
		<input class="flag" type="image" src="${pageContext.request.contextPath}/img/ru.png" height="20" width="40" name="language" value="ru"> 
		<input class="flag" type="image" src="${pageContext.request.contextPath}/img/de.gif" height="20" width="40" name="language" value="de">
	</form></div>
	<br><br><br><br>
</body>
</html>