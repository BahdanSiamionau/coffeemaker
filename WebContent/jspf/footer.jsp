<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/tagslib.tld" prefix="mytag"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" href="css/style.css"
		type="text/css" />
</head>
<body>	
	<fmt:setLocale value="${language}" />
	<fmt:setBundle basename="properties.labels" var="bundle" />
	<br>
	<br>
	<div id="footer">
	<mytag:time_tag />
	<br> <fmt:message key="footer" bundle="${bundle}"/>
	</div>
</body>
</html>