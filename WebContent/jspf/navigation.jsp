<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<link rel="stylesheet" href="css/style.css" type="text/css" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties.labels" var="labels" />
</head>
<body>
	<div id="addition">
		<b>
		<fmt:message key="yourbalance" bundle="${labels}" />
		<br>
		<div id="balance"> 
		<jsp:useBean id="rg" scope="request"
			class="by.siamionau.coffeemaker.dao.UserDAO" />
		<c:forEach var="rguser" items="${rg.users}">
			<c:choose>
				<c:when test="${rguser.login == user}">
					<c:out value="${rguser.balance}"/>
				</c:when>
			</c:choose>
		</c:forEach> </b> <br> <hr>
		</div>
		<c:choose>
			<c:when test="${'admin' == role}">
				<form action="login" method="post">
					<input type="hidden" name="command" value="session"> <input
						type="hidden" name="session" value="replenishment"> <input
						type="submit"
						value=<fmt:message key="replenishment" bundle="${labels}" />>
				</form>
				<form action="login" method="post">
					<input type="hidden" name="command" value="session"> <input
						type="hidden" name="session" value="allissues"> <input
						type="submit"
						value=<fmt:message key="allissues" bundle="${labels}" />>
				</form>
				<form action="login" method="post">
					<input type="hidden" name="command" value="session"> <input
						type="hidden" name="session" value="allusers"> <input
						type="submit"
						value=<fmt:message key="allusers" bundle="${labels}" />>
				</form>
			</c:when>
			<c:when test="${'user' == role}">
			</c:when>
		</c:choose>
		<form action="login" method="post">
			<input type="hidden" name="command" value="session"> <input
				type="hidden" name="session" value="drinks"> <input
				type="submit"
				value="<fmt:message key="takedrink" bundle="${labels}" />">
		</form>
		<form action="login" method="post">
			<input type="hidden" name="command" value="session"> <input
				type="hidden" name="session" value="myissues"> <input
				type="submit"
				value=<fmt:message key="myissues" bundle="${labels}" />>
		</form>
	</div>
</body>
</html>