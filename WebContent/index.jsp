<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<fmt:setBundle basename="properties.labels" var="labels"/>
<title>Sign up form</title>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/languages/jquery.validationEngine-en.js"
	type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript"
	charset="utf-8"></script>
<link rel="stylesheet" href="css/validationEngine.jquery.css"
	type="text/css" />
<link rel="stylesheet" href="css/style.css"
	type="text/css" />
<script>
	$(document).ready(function() {
		$("#formID").validationEngine();
	});
</script>
</head>
<body>
	<div id="header"><img width=90% src="${pageContext.request.contextPath}/img/header.jpg"/></div>
	<form id="formID" action="login" method="post">
	<div id="content"> 
		<br /> 
		<input type="hidden" name="command" value="login">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login: 
		<input class="validate[required, minSize[4], maxSize[16]]" type="text"
			name="login" id="login" /> <br /> 
		<fmt:message key="password" bundle="${labels}"/>
		<input type="password"	name="password"> <br /> 
		<input type="submit" value="Sign up">
	</div>
	</form> 
</body>
</html>