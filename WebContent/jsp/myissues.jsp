<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/tagslib.tld" prefix="mytag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:directive.page errorPage="/jsp/errors/exceptions.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties.labels" var="labels" />
<title><fmt:message key="replenishment" bundle="${labels}" /></title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<div id="header">
		<img width=90% src="${pageContext.request.contextPath}/img/header.jpg" />
	</div>
	<jsp:include page="/jspf/header.jsp" />
	<jsp:include page="/jspf/navigation.jsp" />
	<div id="main">
		<jsp:useBean id="rw" scope="request"
			class="by.siamionau.coffeemaker.dao.OrderDAO" />
		<table width="100%">
			<tr align="center" bgcolor="#999999">
				<td>ID</td>
				<td><fmt:message key="login" bundle="${labels}" /></td>
				<td><fmt:message key="col2name" bundle="${labels}" /></td>
				<td><fmt:message key="colsugar" bundle="${labels}" /></td>
				<td><fmt:message key="issued" bundle="${labels}" /></td>
				<td><fmt:message key="milk" bundle="${labels}" /></td>
				<td><fmt:message key="chocolate" bundle="${labels}" /></td>
			</tr>
			<c:forEach var="order" items="${rw.orders}">
				<c:choose>
					<c:when test="${order.login == user}">
						<tr>
							<td>${order.id}</td>
							<td>${order.login}</td>
							<td>${order.drink}</td>
							<td>${order.sugar}</td>
							<td>${order.issued}</td>
							<td>${order.milk}</td>
							<td>${order.chocolate}</td>
						</tr>
					</c:when>
				</c:choose>
			</c:forEach>

		</table>
	</div>
</body>
<div id="underfooter"></div>
<jsp:include page="/jspf/footer.jsp" />
</body>
</html>