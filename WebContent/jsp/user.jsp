<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/tagslib.tld" prefix="mytag"%>
<jsp:directive.page errorPage="/jsp/errors/exceptions.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties.labels" var="labels" />
<title><fmt:message key="success" bundle="${labels}" /></title>
<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<div id="header">
		<img width=90% src="${pageContext.request.contextPath}/img/header.jpg" />
	</div>
	<jsp:include page="/jspf/header.jsp" />
	<jsp:include page="/jspf/navigation.jsp" />
	<div id="main">
		<fmt:message key="sugar" bundle="${labels}" />
		<form action="login" method="post">
			<input type="hidden" name="command" value="issue"> <input
				type="radio" name="sugar" value="0"> 0 <input type="radio"
				name="sugar" value="1"> 1 <input type="radio" name="sugar"
				value="2" checked> 2 <input type="radio" name="sugar"
				value="3"> 3 <input type="radio" name="sugar" value="4">
			4
			<div id="flags">
				<fmt:message key="additions" bundle="${labels}" />
				<input type="checkbox" name="milk" value="true">
				<fmt:message key="milk" bundle="${labels}" />
				<input type="checkbox" name="chocolate" value="true">
				<fmt:message key="chocolate" bundle="${labels}" />
			</div>
			<br>
			<jsp:useBean id="rw" scope="request"
				class="by.siamionau.coffeemaker.dao.DrinkDAO" />
			<mytag:menu_table>
				<jsp:attribute name="length">${rw.size}</jsp:attribute>
				<jsp:attribute name="secondcolumn">
					<fmt:message key="col2name" bundle="${labels}" />
				</jsp:attribute>
				<jsp:attribute name="thirdcolumn">
					<fmt:message key="col3name" bundle="${labels}" />
				</jsp:attribute>
				<jsp:body>${rw.element}</jsp:body>
			</mytag:menu_table>
		</form>
	</div>
	<div id="underfooter"></div>
	<jsp:include page="/jspf/footer.jsp" />
</body>
</html>