<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:directive.page isErrorPage="true" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="properties.labels" var="labels"/>
<title><fmt:message key="error.message" bundle="${labels}" /></title>
<link rel="stylesheet" href="css/style.css"
	type="text/css" />
</head>
<body>
	<div id="header"><img width=90% src="${pageContext.request.contextPath}/img/header.jpg"/></div>
	<br><fmt:message key="error.generated" bundle="${labels}" /> <br />
	<jsp:expression>exception.toString()</jsp:expression>
	<form action="login" method="post">
		<input type="hidden" name="command" value="logout"> 
		<input type="submit" value=<fmt:message key="back" bundle="${labels}"/>>
	</form>
</body>
</html>