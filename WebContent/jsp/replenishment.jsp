<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/tagslib.tld" prefix="mytag"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:directive.page errorPage="/jsp/errors/exceptions.jsp" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="properties.labels" var="labels" />
<title><fmt:message key="replenishment" bundle="${labels}" /></title>
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/languages/jquery.validationEngine-en.js"
	type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.validationEngine.js" type="text/javascript"
	charset="utf-8"></script>
<link rel="stylesheet" href="css/validationEngine.jquery.css"
	type="text/css" />
<link rel="stylesheet" href="css/style.css"
	type="text/css" />
<script>
	$(document).ready(function() {
		$("#formID").validationEngine();
	});
</script>
</head>
<body>
	<div id="header">
		<img width=90% src="${pageContext.request.contextPath}/img/header.jpg" />
	</div>
	<jsp:include page="/jspf/header.jsp" />
	<jsp:include page="/jspf/navigation.jsp" />
	<div id="main">
		<form id="formID" action="login" method="post">
			<input type="hidden" name="command" value="replenishment">
			<jsp:useBean id="rw" scope="request"
				class="by.siamionau.coffeemaker.dao.ComponentDAO" />
			<mytag:replenishment_table>
				<jsp:attribute name="length">${rw.size}</jsp:attribute>
				<jsp:attribute name="secondcolumn">
					<fmt:message key="col2name" bundle="${labels}" />
				</jsp:attribute>
				<jsp:attribute name="thirdcolumn">
					<fmt:message key="col3name" bundle="${labels}" />
				</jsp:attribute>
				<jsp:attribute name="forthcolumn">
					<fmt:message key="col4name" bundle="${labels}" />
				</jsp:attribute>
				<jsp:body>${rw.element}</jsp:body>
			</mytag:replenishment_table>
		</form>
	</div>
	<div id="underfooter"></div>
	<jsp:include page="/jspf/footer.jsp" />
</body>
</html>